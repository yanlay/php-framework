<?php

$loader = require 'vendor/autoload.php';
$loader->register();

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Framework\Core;

$request = Request::createFromGlobals();

$app = new Core();
    
$app->map('/hello/{name}', function ($name) {
    $response = new Response('Hello '.$name);
    return $response;
});

$app->handle($request);
